var gulp         = require('gulp'), // Подключаем Gulp
    sass         = require('gulp-sass'); //Подключаем Sass пакет,

gulp.task('sass', function(){ // Создаем таск Sass
    return gulp.src('app/sass/**/*.scss') // Берем источник
        .pipe(sass())// Преобразуем Sass в CSS посредством gulp-sass
        .pipe(gulp.dest('app/css')); // Выгружаем результата в папку app/css
       
});
gulp.task('watch', function() {
    gulp.watch('app/sass/**/*.scss', ['sass']); // Наблюдение за sass файлами в папке sass
});